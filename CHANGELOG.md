# Version v0.4.2
- 2021 January 20
- #7 Implementing Proper Unit tests as a replacement/improvement for the command-line tests
- Dropped support for Framework 3.1 due to incompatibility with testing suite.

# Version v0.4.1
- 2021 January 18
- #17 Stopped permitting invalid bets. Followup from #12.
- #19 Hit/Stay logic will now recognize Bust and prevent further hits.
- #20 Apply simply cleanups short of full refactor.

# Version v0.4.0
- 2021 January 17
- #6 Apply property file settings to build project with executable.

# Version v0.3.3
- 2021 January 15
- #16 Use template logic to justify the card printout.
- #12 Working out the betting implementation. Some bugs remain.

# Version v0.3.2
- 2021 January 4
- #14 Resolve "Add colors to Menus and other displays"
- #9 Dealer now draws cards during the NewHand() method.
- #13 Resolve "Implement "Stay" action."

# Version v0.3.1
- 2021 January 3
- Implemented the concept of 'Hit' to the game.
- rework tests to be based on extends keyword.
- #5 Resolve "Compute Score"
- Still need to implement Stay

# Version v0.3.0
- 2022 January 3
- Happy New Year
- Rewrote Menu classes to be more extensible.
- Stubbed out the GameState and GameRound classes
- Separated the Code Quality job to it's own stage.
- Still a lot to do.

# Version v0.2.0
- 2021 November 28
- Set up the Menu and MainMenu classes to handle processing user input.
- Support for dynamically updating the verbosity flag of Menu.

# Version v0.1.0
- 2021 November 27
- Implements the Card struct and the Deck static class.
- Support for creating Standard 52-card decks, Shuffle, and Print added to Deck class.

# Version v0.0.1
- 2021 November 21
- Intitial template of dotnet: Hello World
- Fixed build pipeline to specify a working version of the dotnet SDK (3.1)
- Selected SDK 3.1 because it was already installed to my system. Latest is 6
- Including the code quality template.