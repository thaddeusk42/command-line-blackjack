
using Xunit;
using cmd_blackjack;

namespace cmd_blackjack_tests
{
	public class CardUnitTests
	{
		// Queen of Spades has value 10
		// Eight of Hearts has value 8
		// Ace of Clubs has value 1
		[Theory]
		[InlineData(Ranks.Ace, Suits.Clubs, 1)]
		[InlineData(Ranks.Eight, Suits.Clubs, 8)]
		[InlineData(Ranks.Queen, Suits.Clubs, 10)]
		public void CardHasExpectedValue(Ranks rank, Suits suit, int expected_value)
		{
			var card = new Card(rank, suit);
			int actual_value = card.Value();
			Assert.Equal(expected_value, actual_value);
		}

	}
}