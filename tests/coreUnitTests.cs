
using Xunit;

namespace cmd_blackjack
{
    public class CoreUnitTests
	{
		[Fact]
		// This test is mainly to validate that the basic test framework itself is happy.
		// If this ever dies, it's probably something like a missing dependency 
		// or your test command is confused about what framework/runtime you're trying to invoke.
		public void OneEqualsOne ()
		{
			int m = 1;
			int n = 1; // alter this to force a fail.

			Assert.Equal(m, n);
		}
	}
}