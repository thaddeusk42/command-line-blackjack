
using Xunit;
using cmd_blackjack;
using System.Collections.Generic;

namespace cmd_blackjack_tests
{
	public class DeckUnitTests
	{
		// Shuffled deck has a given card
		[Theory]
		[InlineData(Ranks.Ace, Suits.Clubs)]
		[InlineData(Ranks.Two, Suits.Clubs)]
		[InlineData(Ranks.Three, Suits.Clubs)]
		[InlineData(Ranks.Four, Suits.Clubs)]
		[InlineData(Ranks.Five, Suits.Clubs)]
		[InlineData(Ranks.Six, Suits.Clubs)]
		[InlineData(Ranks.Seven, Suits.Clubs)]
		[InlineData(Ranks.Eight, Suits.Clubs)]
		[InlineData(Ranks.Nine, Suits.Clubs)]
		[InlineData(Ranks.Ten, Suits.Clubs)]
		[InlineData(Ranks.Jack, Suits.Clubs)]
		[InlineData(Ranks.Queen, Suits.Clubs)]
		[InlineData(Ranks.King, Suits.Clubs)]
		public void OrderedDeckHasCard(Ranks rank, Suits suit)
		{
			Card expected = new Card(rank,suit);
			List<Card> deck = Deck.GetOrderedDeck();
			Assert.Contains(expected, deck);
		}

		// Shuffled deck has a given card
		[Theory]
		[InlineData(Ranks.Ace, Suits.Clubs)]
		[InlineData(Ranks.Two, Suits.Clubs)]
		[InlineData(Ranks.Three, Suits.Clubs)]
		[InlineData(Ranks.Four, Suits.Clubs)]
		[InlineData(Ranks.Five, Suits.Clubs)]
		[InlineData(Ranks.Six, Suits.Clubs)]
		[InlineData(Ranks.Seven, Suits.Clubs)]
		[InlineData(Ranks.Eight, Suits.Clubs)]
		[InlineData(Ranks.Nine, Suits.Clubs)]
		[InlineData(Ranks.Ten, Suits.Clubs)]
		[InlineData(Ranks.Jack, Suits.Clubs)]
		[InlineData(Ranks.Queen, Suits.Clubs)]
		[InlineData(Ranks.King, Suits.Clubs)]
		public void ShuffledDeckHasCard(Ranks rank, Suits suit)
		{
			Card expected = new Card(rank,suit);
			List<Card> deck = Deck.GetShuffledDeck();
			Assert.Contains(expected, deck);
		}
	}

}