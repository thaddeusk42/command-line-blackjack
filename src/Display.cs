using System;
using System.IO;

namespace cmd_blackjack
{
	public static class Display
	{
		// Console.ForegroundColor seems to not work in GitBash.
		// Go old-school with ANSI escape codes.
		// Refer: https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html#background-colors
		public static String reset = "\u001b[0m";
		public static String invert = "\u001b[7m";
		public static String amber = "\u001b[33m";
		public static String cyan = "\u001b[36m";
		public static String grey = "\u001b[38;5;244m";

		public static void Reset()
		{
			// Redundant template notation to match rest of file.
			Console.Write("{0}",
				reset
			);
			Console.ResetColor(); // Just a wrapper for now
		}

		public static void Line(String message)
		{
			// Reset is probably unneeded,
			// keep it to be paranoid about missed escapes.
			Console.WriteLine("{0}{1}",
				reset,
				message
			);
		}

		public static void Warn(String message)
		{
			Console.WriteLine("{0}{1}{2}",
				amber,
				message,
				reset
			);
		}

		public static void Menu(String message)
		{
			Console.WriteLine("{0}{1}{2}",
				cyan,
				message,
				reset
			);
		}

		public static void Card(String message)
		{
			Console.WriteLine("{0}{1}{2}",
				invert,
				message,
				reset
			);
		}

		public static void Note(String message)
		{
			Console.WriteLine("{0}{1}{2}",
				grey,
				message,
				reset
			);
		}

	}

	// Might not be needed.
	public enum DisplayModes
	{
		Default,
		Card,
		Menu,
		Alert
	}
}