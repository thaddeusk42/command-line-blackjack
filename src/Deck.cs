using System;
using System.Collections.Generic;

namespace cmd_blackjack
{
	public static class Deck
	{
		static bool verbose = false ; // noisy console logging if true.

		public static List<Card> GetOrderedDeck()
		{
			List<Card> standard_deck = new List<Card>();

			foreach (Suits suit in (Suits[]) Enum.GetValues(typeof(Suits)))
			{
				foreach (Ranks rank in (Ranks[]) Enum.GetValues(typeof(Ranks)))
				{
					Card card = new Card( rank, suit);
					if (verbose) {
						// Noisy logging.
						Console.WriteLine("( " + rank + " of " + suit + " )");
					}
					standard_deck.Add(card);
				}
			}

			return standard_deck;
		}

		public static void Print(List<Card> deck)
		{
			if (deck.Count == 0)
			{
				Console.WriteLine("\tNo cards");
			}
			foreach (Card card in deck)
			{
				if (verbose)
				{
					Console.WriteLine("[ {0} of {1} ] worth {2}", card.Rank, card.Suit, card.Value());
				}
				else
				{
					Print(card);
				}
			}
		}

		public static void Print(Card card)
		{
			// Due to template construction, we end up with a somewhat messy print that invokes the hardcodes of Display.
			Console.WriteLine("{2}[ {0,5} of {1,-9} ]{3}", card.Rank, card.Suit, Display.invert, Display.reset);
		}

		public static void Shuffle( List<Card> deck)
		{
			Display.Note("\tThe dealer shuffles the deck.");
			// Reference: https://en.wikipedia.org/wiki/Fisher–Yates_shuffle#The_modern_algorithm
			var dice = new Random();
			for ( int i = deck.Count -1; i > 0; i--)
			{
				int roll = dice.Next(deck.Count);
				if (verbose) {
					Display.Note("swap: " + i + "," + roll);
				}
				// manual swap
				var sleeve = deck[i];
				deck[i] = deck[roll];
				deck[roll] = sleeve;
			}
		}

		public static List<Card> GetShuffledDeck()
		{
			var deck = GetOrderedDeck();
			Shuffle(deck);
			return deck;
		}

		public static Card? DrawTo( List<Card> source, List<Card> target)
		{
			// For some reason List<> doesn't implement Pop(), so do it manually.

			// nullguard.
			if (source.Count == 0)
			{
				return null;
			}

			var drawn = source[0];
			source.RemoveAt(0);
			if (verbose) {Print(drawn); }
			target.Add(drawn);
			return drawn; // Invoker gets a 'duplicate' of the card for display reasons.
		}

		public static int ScoreHand( List<Card> hand)
		{
			// nullguard
			if (hand.Count == 0) { return 0; }

			int tally = 0;
			bool has_ace = false;
			foreach ( Card card in hand )
			{
				tally += card.Value(); // sum of values
				// Aces can optionally be 11.
				// since two Aces at 11 would be 22 (bust),
				// we only need the first ace to act that way.
				if (card.Rank == Ranks.Ace) { has_ace = true; }
			}

			// If upgrading won't bust you, and you have an ace,
			// count it as 11. We already included 1, so add the 10.
			bool ace_upgrades = (has_ace) && (tally <= 11);
			if (ace_upgrades) { tally += 10; }

			return tally;
		}
	}
}