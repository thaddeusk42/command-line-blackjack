using System;
using System.IO;

namespace cmd_blackjack
{
	public class BetMenu : Menu
	{
		// The betting menu is pretty different from other menus.
		// Mostly that we take in integer input instead of a menu option.
		protected String[] dynamic_messages = {
			"  B : Place a bet, and continue playing.",
			"  F : Fold and Cash Out."
		};

		protected GameState gameState;

		public BetMenu(GameState state)
		{
			gameState = state; // Pass by reference is the intent here.
		}

		override public MenuOptions Update()
		{
			DisplaySpecificOptions(dynamic_messages);
			var selection = ReadSelection();

			return ProcessSelection(selection);
		}

		override protected MenuOptions ProcessSelection( ConsoleKey selection )
		{

			MenuOptions option;

			switch (selection)
			{
				case ConsoleKey.B:
					option = MenuOptions.Bet;

					break;
				case ConsoleKey.F:
					option = MenuOptions.Quit;

					break;
				default:
					// Fallback to the root menu.
					option = base.ProcessSelection( selection );
					break;
			}

			return option;
		}

		private void DisplayBetRequest()
		{

			String chips_message = "\tYou hold chips valuing: {0}{1}." ;

			String bet_message =
				Display.cyan +
				"Place a bet between {0} and {1}." +
				"\t Your previous bet was " +
				Display.reset +
				"{2}."  ;

			Console.WriteLine(
				chips_message,
				gameState.currencySymbol,
				gameState.currency);

			Console.WriteLine(
				bet_message,
				gameState.minBet,
				gameState.maxBet,
				gameState.playerBet);
		}

		// ReadInteger - self-managed loop
		// Maybe we should move this to the superclass.
		private int? ReadInteger()
		{
			while ( invalidMenuSelections < maxInvalidSelections)
			{
				String raw = cIn.ReadLine(); // Refer to parent

				int result;

				bool success = Int32.TryParse( raw, out result);

				if (success)
				{
					return result;
				}
				else
				{
					invalidMenuSelections++;
					DisplayInvalidSelectionMessage();
				}
			}

			// Fail out with null response.
			return null;
		}

		public int RequestBet()
		{
			DisplayBetRequest();
			Console.Write(" > ");
			int? selection = ReadInteger();

			// if not a number, default to minimum bet and return.
			if (selection == null)
			{
				Display.Note("That didn't look like an integer.");
				Display.Warn("Defaulting to minimum bet.");
				return gameState.minBet;
			}

			// Wrapper function for checking selection against game state limits.
			return ClampBet((int)selection);
		}

		private int ClampBet(int selection)
		{
			// if below minimum - then default to min and return.
			// this also protects against negatives provided minBet is positive..
			if (selection < gameState.minBet)
			{
				var min_message = "This table's minimum bet is: " + gameState.minBet;
				Display.Note(min_message);
				Display.Warn("Defaulting to minimum bet.");
				selection = gameState.minBet;
			}
			// if above max - then default to max and return
			else if (selection > gameState.maxBet)
			{
				var max_message = "This table's maximum bet is: " + gameState.maxBet;
				Display.Note(max_message);
				Display.Warn("Defaulting to maximum bet.");
				selection = gameState.maxBet;
			}
			// if above current - then go All In and return.
			
			if (selection >= gameState.currency)
			{
				Display.Note("That's more than you have.");
				Display.Warn("Going All In.");
				Display.Note("Good luck.");
				selection = gameState.currency;

			}

			// finally - return selection.
			return selection;
		}
	}
}