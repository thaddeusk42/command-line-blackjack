using System;
using System.IO;

namespace cmd_blackjack
{
	public class HandMenu : Menu
	{
		protected String[] dynamic_messages = {
			"  H : Hit (Draw an additional card)",
			"  S : Stay (Commit your current hand for scoring)"
		};

		protected GameState gameState;

		public HandMenu(GameState state)
		{
			gameState = state; // Pass by reference is the intent here.
		}

		override public MenuOptions Update()
		{
			DisplaySpecificOptions(dynamic_messages);
			var selection = ReadSelection();

			return ProcessSelection(selection);
		}


		override protected MenuOptions ProcessSelection( ConsoleKey selection )
		{
			MenuOptions option;
			switch (selection)
			{
				case ConsoleKey.H:
					// Draw new Card.
					option = MenuOptions.Hit;
					base.invalidMenuSelections = 0;
					break;
				case ConsoleKey.S:
					// Process Round End.
					option = MenuOptions.Stay;
					base.invalidMenuSelections = 0;
					break;
				default:
					// Fallback to the root menu.
					option = base.ProcessSelection( selection );
					break;
			}
			return option;
		}

		protected override void DisplayQuitMessage()
		{
			// Later work will write this more dynamically.
			var message = "Aborting Game.\tReturning to Main Menu.";
			Console.WriteLine(message);
		}
	}
}