namespace cmd_blackjack
{
	// Despite the fact any given menu doesn't use most of these, share the top level enum publicly.
	public enum MenuOptions
	{
		Invalid,
		Quit,
		Verbose,
		Settings,
		NewGame,
		Hit,
		Stay,
		Bet
	}
}