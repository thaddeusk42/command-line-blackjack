using System;
using System.IO;

namespace cmd_blackjack
{
	abstract public class Menu
	{
		static protected bool verbose = false ; // noisy console logging if true.
		// Note that this instance of verbose is different from other classes, so verbosity is class-specific.

		protected TextReader cIn = Console.In;
		protected TextWriter cOut = Console.Out;
		protected int invalidMenuSelections = 0;
		protected int maxInvalidSelections = 10;

		abstract public MenuOptions Update();

		// The basic one only has Quit.
		protected String[] messages_start = {
			Display.cyan + "Please select an option:"+ Display.reset
		};
		protected String[] messages_finish = {
			"  V : toggle verbose mode.",
			"  Q : quit the game."
		};

		protected virtual MenuOptions ProcessSelection( ConsoleKey selection )
		{
			MenuOptions option;
			switch (selection)
			{
				case ConsoleKey.Q:
					option = MenuOptions.Quit;
					DisplayQuitMessage();
					break;
				case ConsoleKey.V:
					option = MenuOptions.Verbose;
					verbose = !verbose;
					DisplayVerboseState();
					break;
				default:
					// maintains count so we can fail.
					option = ProcessInvalid();
					break;
			}
			return option;
		}

		protected MenuOptions ProcessInvalid()
		{
				var option = MenuOptions.Invalid;
				DisplayInvalidSelectionMessage();
				invalidMenuSelections++;
				if (invalidMenuSelections > maxInvalidSelections)
				{
					option = MenuOptions.Quit;
					DisplayQuitMessage();
				}
				return option;
		}

		// Wrapper for handling potential weirdness with key-reading.
		// Forces upper case for handling, anything bizzare is treated as a newLine character.
		protected ConsoleKey ReadSelection()
		{
			var line = cIn.ReadLine();
			// nullguard
			bool invalid = (line == null || line.Length < 1);
			if (invalid) { line = "\n"; } // default to a newline.

			// upperguard
			var ascii = line[0];
			ascii = Char.ToUpper(ascii);

			if (verbose) {
				Console.WriteLine("\t ReadSelection() got: {0}" , ascii);
			}

			ConsoleKey key = (ConsoleKey)ascii;
			return key;
		}

		protected void DisplayVerboseState()
		{
			var verbosity = (verbose ? "loud" : "quiet");
			var message = "\tThe current verbosity state is now: {0}." ;
			Console.WriteLine(message, verbosity);
		}

		protected void DisplayInvalidSelectionMessage()
		{
			// Later work will write this more dynamically.
			var message = "\tYour attempted selection was not valid.";
			Display.Warn(message);
		}

		protected virtual void DisplayQuitMessage()
		{
			// Later work will write this more dynamically.
			var message = "\tThank you for playing.";
			Console.WriteLine(message);
		}

		// Because various types of menus want to be able
		// to edit the options, we take in a dynamic list.
		// We also want to still work without it, so we set up a
		// null default as per https://stackoverflow.com/a/8213790
		protected void DisplayAvailibleOptions(String[] dynamic_options = null)
		{
			Console.Write("\n");
			// Later work will write this more dynamically.
			foreach (String message in messages_start)
			{
				Console.WriteLine(message);
			}

			if (dynamic_options != null )
			{
				foreach (String message in dynamic_options)
				{
					Console.WriteLine(message);
				}
			}

			foreach (String message in messages_finish)
			{
				Console.WriteLine(message);
			}

			Console.Write(" > ");
		}

		protected void DisplaySpecificOptions(String[] options)
		{
			Console.Write("\n");

			foreach (String message in messages_start)
			{
				Console.WriteLine(message);
			}

			foreach (String message in options)
			{
				Console.WriteLine(message);
			}

			Console.Write(" > ");
		}

	}

}