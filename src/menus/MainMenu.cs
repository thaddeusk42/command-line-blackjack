using System;
using System.IO;

namespace cmd_blackjack
{
	public class MainMenu : Menu
	{

		protected String[] dynamic_messages = {
			"  N : Begin a new game.",
			"  S : Access Settings."
		};

		override public MenuOptions Update()
		{
			DisplayAvailibleOptions(dynamic_messages);
			var selection = ReadSelection();

			return ProcessSelection(selection);
		}

		override protected MenuOptions ProcessSelection( ConsoleKey selection )
		{

			MenuOptions option;

			switch (selection)
			{
				case ConsoleKey.N:
					option = MenuOptions.NewGame;
					DisplayNewGameMessage();
					break;
				case ConsoleKey.S:
					option = MenuOptions.Settings;
					// DisplaySettingsMenu();
					break;
				default:
					// Fallback to the root menu.
					option = base.ProcessSelection( selection );
					break;
			}

			return option;
		}

		protected void DisplayNewGameMessage()
		{
			// Later work will write this more dynamically.
			var message = "\tStarting a new Game....";
			Console.WriteLine(message);
		}

	}
}