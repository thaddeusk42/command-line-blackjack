using System;


namespace cmd_blackjack
{
	public struct Card : IEquatable<Card>
	{
		// Auto-implemented readonly property:
		public Ranks Rank { get; }
		public Suits Suit { get; }
		// Also a Value described below.

		public Card( Ranks rank, Suits suit)
		{
			Rank = rank;
			Suit = suit;
		}

		public int Value ()
		{
			// Returning directly out of the switch to keep it shorter.
			switch(this.Rank)
			{
				case Ranks.Ace:
					return 1; // the 11 case is handled elsewhere.
				case Ranks.King:
				case Ranks.Queen:
				case Ranks.Jack:
					return 10;
				default:
					return (int)this.Rank;
			}
		}

		//
		public bool Equals(Card other)
		{
			// if (other == null) { return false; } // null guard
			// Equal if suit and rank are the same.
			return (this.Rank == other.Rank) && (this.Suit == other.Suit);
		}
	}

	public enum Ranks
	{
		Ace, // Can be 1 OR 11.
		Two = 2,
		Three = 3,
		Four = 4,
		Five = 5,
		Six = 6,
		Seven = 7,
		Eight = 8,
		Nine = 9,
		Ten = 10,
		Jack, // these still score as 10 but we don't want int collision on the enums.
		Queen,
		King
	}

	public enum Suits
	{
		Hearts,
		Diamonds,
		Clubs,
		Spades
	}
}