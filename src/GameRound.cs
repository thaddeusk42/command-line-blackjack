using System;
using System.IO;

namespace cmd_blackjack
{
	public class GameRound {
		protected GameState gameState;
		protected HandMenu handMenu;
		protected BetMenu betMenu;
		bool verbose = false ; // noisy console

		public GameRound()
		{
			gameState = new GameState(startingCurrency: 100);
			handMenu = new HandMenu(gameState);
			betMenu = new BetMenu(gameState);
		}

		// Each Round should be one Update at this level.
		public MenuOptions Update()
		{
			var action = DetermineRoundAction();
			switch (action)
			{
				case MenuOptions.Bet:
					TakeBets();
					HandLoop();
					break;
				case MenuOptions.Quit:
					DisplayFoldMessage();
					return action;
				case MenuOptions.Verbose:
					verbose = !verbose;
					break;
				default:
					break;
			}

			return action;
		}

		private MenuOptions DetermineRoundAction()
		{
			// check for forced fold.
			bool forced_fold = gameState.currency < gameState.minBet;

			if (forced_fold) {
				Display.Warn("\tThe chips you hold are less than this tables minimum bet.");
				return MenuOptions.Quit;
			}
			// check if player wants to bet.
			else { return betMenu.Update(); }
		}

		private void HandLoop()
		{
			// Process the logic of a single hand, not counting it's bets.
			var action = MenuOptions.Bet; // Not a valid selection here, used to init the action.
			NewHand();
			do
			{
				DisplayKnownInfo();
				action = DetermineHandAction();
				Console.Write("\n");
				switch (action)
				{
					case MenuOptions.Hit:
						PlayerTakeHit();
						break;
					case MenuOptions.Stay:
						ResolveHand();
						break;
					case MenuOptions.Verbose:
						verbose = !verbose;
						break;
					default:
						break;
				}
			} while (action != MenuOptions.Stay);
		}

		private MenuOptions DetermineHandAction()
		{
			int player_score = Deck.ScoreHand(gameState.playerHand);
			bool forced_stay = player_score >= 21; // "Naturals" also force a stay.

			if (forced_stay) { return MenuOptions.Stay; }
			// else
			return handMenu.Update();
		}

		public void ResolveHand()
		{
			Display.Warn("\nAll Players have chosen to Stand.");

			int dealer_score = Deck.ScoreHand(gameState.dealerHand);
			int player_score = Deck.ScoreHand(gameState.playerHand);

			// Check for Bust.
			if (player_score < 21)
			{
				// Player is not yet Bust.
				// Dealer will continue to Hit until their score is 17 or more.

				Display.Line("\tThe Dealer reveals their face down card");
				Deck.Print(gameState.dealerHand[1]);

				while (dealer_score < 17)
				{
					DealerTakeHit(visible: true);
					dealer_score = Deck.ScoreHand(gameState.dealerHand);
				}
			}

			DisplayHandEndStatus(dealer_score, player_score);

			ResolveBets();
			Display.Menu("Press enter to continue.");
			Console.In.ReadLine(); // Don't care about the key,
			// just want to make sure the player stops to see the results.

		}

		public void ResolveBets()
		{
			// Since the bet was already taken, we only need to check
			// for a clean player victory.
			int dealer_score = Deck.ScoreHand(gameState.dealerHand);
			int player_score = Deck.ScoreHand(gameState.playerHand);

			// Going for clarity of logic over efficiency.
			bool player_bust = (player_score > 21);
			bool dealer_bust = (dealer_score > 21);
			bool player_high = (player_score > dealer_score);
			bool player_tied = (player_score == dealer_score);

			// If Player went bust they don't get any winnings.
			if (player_bust) { return ; }
			else if ( dealer_bust || player_high)
			{
				// Pay bet.
				var winnings = gameState.playerBet * 2;
				var message = "The dealers pays you {0}{1}, and you recover your stake.";
				Console.WriteLine(message, gameState.currencySymbol, gameState.playerBet);

				gameState.currency += winnings;
			}
			else if (player_tied)
			{
				// Pay bet.
				var winnings = gameState.playerBet;
				var message = "You recover your stake.";
				Console.WriteLine(message);

				gameState.currency += winnings;

			}

			DisplayChips();
		}

		public void DisplayHandEndStatus(int dealer_score, int player_score)
		{
			DisplayDealersHand(all: true);
			Console.WriteLine("\tWhich sum to {0} ", dealer_score);

			DisplayPlayersHand();

			// this probably has to move again when we resolve bets.
			if (player_score > 21)
			{
				Display.Warn("\nYou have gone bust, and lose your bet.");
			}
			else if(dealer_score > 21)
			{
				Display.Warn("\nThe dealer has gone bust.");
			}
			else if ( dealer_score == player_score )
			{
				Display.Warn("\nThe dealer tied with you.");
			}
			else if ( dealer_score > player_score )
			{
				Display.Warn("\nThe dealer won this hand.");
			}
			else
			{
				Display.Warn("\nYou have won this hand.");
			}
		}

		public void NewHand()
		{
			DiscardHands();

			Display.Menu("\n\tStarting a new hand.");

			// Player Hits twice.
			PlayerTakeHit();
			PlayerTakeHit();

			// Dealer Hits publicly.
			DealerTakeHit(visible: true);
			// Dealer Hits privately.
			DealerTakeHit(visible: false);
		}

		public void DiscardHands()
		{
			gameState.playerHand.Clear();
			gameState.dealerHand.Clear();
		}

		public void DisplayPlayersHand()
		{
			Console.WriteLine("You are now holding:");
			Deck.Print(gameState.playerHand);

			var score = Deck.ScoreHand(gameState.playerHand);
			var message = "\tYou hold cards that sum to {0}" ;
			Console.WriteLine(message, score);
		}

		public void DisplayDealersHand(bool all)
		{
			Console.WriteLine("\n\tThe Dealer holds:");
			if ( all) {
				Deck.Print(gameState.dealerHand);
			}
			else if (gameState.dealerHand.Count == 0)
			{
				// The Print list form gracefully handles zero.
				Deck.Print(gameState.dealerHand);
			}
			else
			{
				// have to zero-guard this invocation.
				Deck.Print(gameState.dealerHand[0]);
				Display.Note("[A facedown card]");
			}
		}

		protected void DisplayKnownInfo()
		{
			DisplayChips();
			DisplayStake();
			DisplayPlayersHand();
			DisplayDealersHand(all: false);
		}

		protected void DisplayChips()
		{
			var message = "\tYou hold chips valuing: {0}{1}." ;
			Console.WriteLine(message, gameState.currencySymbol, gameState.currency);
		}

		protected void DisplayStake()
		{
			var message = "\tYou staked  {0}{1} on the current hand.";
			Console.WriteLine(message, gameState.currencySymbol, gameState.playerBet);
		}

		private void PlayerTakeHit()
		{
			var message = "\tThe dealer passes you a card." ;
			Display.Note(message);

			var card = Deck.DrawTo(gameState.theDeck, gameState.playerHand);

			if (card == null)
			{
				RefreshDeck();
				card = Deck.DrawTo(gameState.theDeck, gameState.playerHand);
			}

			if(verbose) {
				Console.Write("You received the ");
				Deck.Print((Card)card); // can't print nullable
			}
		}

		private void DealerTakeHit (bool visible)
		{
			var faceup = visible ? "faceup" : "facedown";
			var message = "\tThe dealer takes a card " + faceup + ".";
			Display.Note(message);

			var card = Deck.DrawTo(gameState.theDeck, gameState.dealerHand);

			if (card == null)
			{
				RefreshDeck();
				card = Deck.DrawTo(gameState.theDeck, gameState.playerHand);
			}

			if(verbose && visible)
			{
				Console.Write("It was the ");
				Deck.Print((Card)card); // can't print nullable
			}
		}

		private void RefreshDeck()
		{
			Console.WriteLine("The deck is empty, so the dealer gets a new one.");
			gameState.theDeck = Deck.GetShuffledDeck();
		}

		public void TakeBets()
		{
			int bet = betMenu.RequestBet();
			gameState.playerBet = bet;
			gameState.currency -= bet;
		}

		private void DisplayFoldMessage()
		{
			// Refer: https://stackoverflow.com/a/19988882
			// The format notation with -8 is for padding control.
			String message = "\n\n\n" +
				"You collect your chips and walk away from the table." +
				"\n\tThese chips value in at: {0}{1, -8}" +
				"\n\tAt the start of the game, you had: {0}{2, -8}";

			// Embedding color controls into the integer.
			// It's a rather hacky move we get to do with strings.
			String current_money =
				Display.amber +
				gameState.currency +
				Display.reset ;

			String initial_money =
				Display.cyan +
				gameState.startCurrency +
				Display.reset ;

			Console.WriteLine(message, gameState.currencySymbol, current_money, initial_money);
		}
	}
}