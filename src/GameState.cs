using System.Collections.Generic;

namespace cmd_blackjack
{
	// NOTE: this is a class purely so that it naturally passes by reference.
	// Please treat it as struct-like in the sense it is a store of variables,
	// and should not have methods other than constructors.
	// This whole struct is effectively the list of variables
	// we want to stay in place for the duration of a game.
	// The functions that manipulate these values mainly live in GameRound and Deck.
	// If this game was designed for multiplayer, we'd probably have a Player struct as well,
	// 	move their hands and bets to there.
	public class GameState {
		// Card related variables.
		public List<Card> theDeck;
		public List<Card> playerHand;
		public List<Card> dealerHand;
		// Currency related variables.
		public int currency;
		public int startCurrency;
		public char currencySymbol;
		public int playerBet; //Arguably only lasts one hand, but we're not handling single hands as a struct.
		public int minBet;
		public int maxBet;

		// Refer: https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/named-and-optional-arguments
		// Using optional, named arguments here to make life easier elsewhere.
		public GameState(
			int startingCurrency = 100,
			int mimimumBet = 5,
			int maximumBet = 100,
			char currencyCharacter = '$',
			int openingBet = 10
		)
		{
			theDeck = Deck.GetShuffledDeck();
			playerHand = new List<Card>();
			dealerHand = new List<Card>();
			currency = startingCurrency;
			startCurrency = startingCurrency;
			currencySymbol = currencyCharacter;
			minBet = mimimumBet;
			maxBet = maximumBet;
			playerBet = openingBet;

		}
	}
}