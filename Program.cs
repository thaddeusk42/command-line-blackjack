using System;
using System.Collections.Generic;

namespace cmd_blackjack
{
	class Program
	{
		static bool verbose = false ; // noisy console logging if true.
		static void Main(string[] args)
		{
			Display.Line("Welcome to");
			Display.Warn("\tCommand Line Blackjack\n"); // Warn for yellow color.
			// if (verbose) { SampleDeck(); }
			if (verbose) { CardDeck(); }
			var mainMenu = new MainMenu();
			var selection = mainMenu.Update();
			while (selection != MenuOptions.Quit)
			{
				if ( selection == MenuOptions.NewGame )
				{
					Play(); // blocks until done playing.
				}
				selection = mainMenu.Update();
			}
		}

		static void Play()
		{
			var game_round = new GameRound();
			var selection = game_round.Update();
			int round_counter = 1;

			while (selection != MenuOptions.Quit)
			{
				if (verbose) {Console.WriteLine("round_counter = {0}", round_counter);}
				// This is where most of the game logic actually runs.
				selection = game_round.Update();
				round_counter++;
			}
		}

		static void CardDeck(){
			List<Card> standard_deck = Deck.GetOrderedDeck();

			// Deck.Print(standard_deck);
			Deck.Shuffle(standard_deck);
			Deck.Print(standard_deck);
		}

		// Demo function originally used to organize thoughts.
		// Doesn't really need to be kept around at this point.
		static void SampleDeck()
		{
			string[] suits = {"Hearts","Diamonds","Clubs","Spades" };
			string[] ranks = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King" };
			// string[] suits = {"♥","♦","♣","♣" }; // Does not appear to print under GitBash. TODO: debug this.

			for (int i = 0 ; i < suits.Length ; i++) {
				for (int j = 0 ; j < ranks.Length ; j++) {
					Console.WriteLine("> " + ranks[j] + " of " + suits[i]);
				}
			}
		}
    }
}
